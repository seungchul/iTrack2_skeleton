package nclab.kaist.edu.pada_itrack2.exception;

/**
 * This class is to describe an exception occurred from permission errors.
 */
public class PermissionException extends RuntimeException {
}
