package nclab.kaist.edu.pada_itrack2.elements;

import android.bluetooth.BluetoothDevice;

/**
 * Bluetooth beacon
 * <br> - It is a tuple of BluetoothDevice and its RSSI value on Bluetooth discovery.
 */
public class Beacon {
    private BluetoothDevice device;
    private short RSSI;
    public Beacon(BluetoothDevice device, short RSSI) {
        this.device = device;
        this.RSSI = RSSI;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public short getRSSI() {
        return RSSI;
    }
}
