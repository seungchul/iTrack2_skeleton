package nclab.kaist.edu.pada_itrack2;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import nclab.kaist.edu.pada_itrack2.exception.PermissionException;

/**
 * Some configurations and global functions.
 * <br> - You may not need to modify this class.
 */
public class Config {
    public static final String TAG = "itrack2.debug";
    public static final String PREF_NAME = "itrack2.pref";
    public static final String PREF_IS_LOGGING_ON = "itrack2.isLoggingOn";

    public static final String INTENT_MOVEMENT_ACTION = "edu.kaist.nclab.itrack2.movement.action";
    public static final String INTENT_INDOOR_ACTION = "edu.kaist.nclab.itrack2.indoor.action";
    public static final String INTENT_OUTDOOR_ACTION = "edu.kaist.nclab.itrack2.outdoor.action";

    public static String LOG_FILE_NAME = "";

    // time parameters in milliseconds
    public static int MOVEMENT_DETECTION_INTERVAL = 10000;
    public static long MICRO_MOVEMENT_DETECTION_WINDOW = 1000;
    public static long MACRO_MOVEMENT_DETECTION_WINDOW = 3000;
    public static double MICRO_MOVEMENT_DETECTION_THRESHOLD = 0.01;
    public static double MACRO_MOVEMENT_DETECTION_THRESHOLD = 0.01;
    public static int BT_BEACON_DETECTION_INTERVAL = 60000;
    public static int GPS_LOCATION_UPDATE_INTERVAL = 60000;

    // other parameters
    public static int GPS_MIN_DISTANCE_TO_UPDATE = 0; // in meters
    public static String[] BT_BEACONS = {"BC:F5:AC:4E:FF:E4", "F8:32:E4:0D:17:8A"};

    public static boolean isMatchingBeacon(String address) {
        for (String beaconAddress : BT_BEACONS) {
            if (address.equals(beaconAddress)) {
                return true;
            }
        }
        return false;
    }

    // Manifest.permission.XXX
    public static void checkPermission(Context context, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            Log.e("PermissionCheck", "Permission is not granted.");
            throw new PermissionException();
        }
    }

    public static String getCurrentTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);
        Date currentTime = new Date();
        String timeStr = simpleDateFormat.format(currentTime);
        return timeStr;
    }

    public static void writeToFile(String fileName, String body) {
        FileWriter fileWriter = null;
        boolean isNewlyCreated = false;

        try {
            final File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/iTrack2/");

            if (!directory.exists()) {
                directory.mkdirs();
            }

            final File logFile = new File(directory, fileName + ".txt");

            if (!logFile.exists()) {
                logFile.createNewFile();
                isNewlyCreated = true;
            }

            fileWriter = new FileWriter(directory.getAbsolutePath() + "/" + fileName + ".txt", true);

            if (isNewlyCreated) {
                fileWriter.write("#iTrack2 log file" + "\r\n");
                fileWriter.write("#indoor row format: type,time,address,name,rssi" + "\r\n");
                fileWriter.write("#outdoor row format: type,time,latitude,longitude,alt,desc" + "\r\n");
            }

            fileWriter.write(body);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
