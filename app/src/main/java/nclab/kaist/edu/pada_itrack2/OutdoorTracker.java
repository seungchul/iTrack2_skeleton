package nclab.kaist.edu.pada_itrack2;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

/**
 * Outdoor tracker based on GPS location requests
 * <br> - You may need to implement periodic GPS location requests.
 * <br> - You may utilize LocationManager's functionality.
 */
public class OutdoorTracker {
    private static Context context = null;
    private static boolean isTrackingOn = false;

    /**
     * It returns whether the tracker is currently running or not.
     *
     * @return
     */
    public static boolean isOutdoorTrackerRunning() {
        return isTrackingOn;
    }

    /**
     * It initiates periodic GPS location tracking to log user traces in outdoor.
     *
     * @param context Android activity or service to bind some periodic actions
     */
    public static void startTracking(Context context) {
        if (!isTrackingOn) {
            OutdoorTracker.context = context;

            /* TODO: Periodic Outdoor Location Tracking
             * Use GPS location update functions to continuously track a user's location
             * It may use locationListener to listen to location updates
             */

            isTrackingOn = true;
        }
    }

    /**
     * It terminates periodic GPS location tracking
     */
    public static void stopTracking() {
        if (isTrackingOn) {
            Config.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

            /* TODO: Stop Periodic Outdoor Location Tracking
             * Here you needs to stop your periodic tracking functionality
             * You may need to check a NULL context
             */

            isTrackingOn = false;
            context = null;
        }
    }

    /**
     * Location listener to track user's outdoor trace
     */
    private static LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            logOutdoorLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * This function logs outdoor location traces into a log file.
     * <br> - You may not need to modify this function.
     *
     * @param location updated user location
     */
    private static void logOutdoorLocation(Location location) {
        SharedPreferences pref = context.getSharedPreferences(Config.PREF_NAME, Context.MODE_PRIVATE);

        if (pref.getBoolean(Config.PREF_IS_LOGGING_ON, false)) {
            // Logging
            String timeStr = Config.getCurrentTime();

            if (location != null) {
                Config.writeToFile(Config.LOG_FILE_NAME, "outdoor," + timeStr + "," + location.getLatitude() + ","
                        + location.getLongitude() + "," + location.getAltitude() + "," + location.getAccuracy() + "\r\n");
                Log.d(Config.TAG, "written: outdoor," + timeStr + "," + location.getLatitude() + ","
                        + location.getLongitude() + "," + location.getAltitude() + "," + location.getAccuracy());
            }
        }
    }
}
