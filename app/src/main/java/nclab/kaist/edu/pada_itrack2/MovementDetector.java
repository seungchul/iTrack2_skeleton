package nclab.kaist.edu.pada_itrack2;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import nclab.kaist.edu.pada_itrack2.elements.AccData;
import nclab.kaist.edu.pada_itrack2.elements.AccelerometerListener;
import nclab.kaist.edu.pada_itrack2.elements.Beacon;
import nclab.kaist.edu.pada_itrack2.elements.OnBluetoothDiscoveryListener;

/**
 * Movement detector based on accelerometer fluctuation
 * <br> - You may need to implement periodic (and continuous) accelerometer sensing.
 */
public class MovementDetector {
    private static Context context = null;
    private static boolean isTrackingOn = false;
    private static boolean isDetectionInProcess = false;
    private static AccelerometerListener accelerometerListener;

    /**
     * It initiates periodic accelerometer-based movement detection.
     *
     * @param context Android activity or service to bind some periodic actions
     */
    public static void startDetection(Context context) {
        if (!isTrackingOn) {
            MovementDetector.context = context;
            accelerometerListener = new AccelerometerListener(context);

            /* TODO: Periodic Movement Detector
             * Use accelerometer readings to periodically detect a user's movement
             */

            isTrackingOn = true;
        }
    }

    /**
     * It terminates periodic movement detection.
     */
    public static void stopDetection() {
        if (isTrackingOn) {
            IndoorTracker.stopTracking();
            OutdoorTracker.stopTracking();

            /* TODO: Stop Periodic Movement Detector
             * Here you needs to stop your periodic detection functionality
             * You may need to check a NULL context
             */

            isTrackingOn = false;
            context = null;
        }
    }

    /**
     * It collects accelerometer data for a designated time.
     * <br> - Use Config.MICRO_MOVEMENT_DETECTION_WINDOW to specify the duration of data collection.
     * <br> - You may need to use accelerometerListener for convenience, as it automatically collects data on onStart(),
     * <br> - After collection, the function should call detectMicroMovement(accDataList).
     */
    public static void collectAccDataForMicro() {
        if (!isDetectionInProcess) {
            isDetectionInProcess = true;

            /* TODO: Collection of accelerometer data for micro movement
             * Use accelerometerListener to collect accelerometer data for a time.
             * You may call detectMicroMovement() after collecting accelerometer data
             */

        }
    }

    /**
     * This function determines whether there is phone's movement or not.
     *
     * @param accDataList ArrayList of AccData. Easily obtained by using AccelerometerListener.getAccList().
     */
    public static void detectMicroMovement(ArrayList<AccData> accDataList) {
        double acEnergy = getAccEnergy(accDataList);

        if (acEnergy > Config.MICRO_MOVEMENT_DETECTION_THRESHOLD) {
            if (!IndoorTracker.isIndoorTrackerRunning() && !OutdoorTracker.isOutdoorTrackerRunning()) {
                Log.d(Config.TAG, "new microMovement found. Start detection in macro-scale.");
                collectAccDataForMacro();
            } else {
                isDetectionInProcess = false;
            }
        } else {
            if (IndoorTracker.isIndoorTrackerRunning() || OutdoorTracker.isOutdoorTrackerRunning()) {
                Log.d(Config.TAG, "possible stationary found. Start detection in macro-scale.");
                collectAccDataForMacro();
            } else {
                isDetectionInProcess = false;
            }
        }
    }

    /**
     * It collects accelerometer data for a designated time.
     * <br> - Use Config.MACRO_MOVEMENT_DETECTION_WINDOW to specify the duration of data collection.
     * <br> - You may need to use accelerometerListener for convenience, as it automatically collects data on onStart(),
     * <br> - After collection, the function should call detectMacroMovement(accDataList).
     */
    public static void collectAccDataForMacro() {

        /* TODO: Collection of accelerometer data for macro movement
         * Use accelerometerListener to collect accelerometer data for a time.
         * You may call detectMicroMovement() after collecting accelerometer data
         */
    }

    /**
     * This function determines whether there is user's movement or not.
     *
     * @param accDataList ArrayList of AccData. Easily obtained by using AccelerometerListener.getAccList().
     */
    public static void detectMacroMovement(ArrayList<AccData> accDataList) {
        double acEnergy = getAccEnergy(accDataList);

        if (acEnergy > Config.MACRO_MOVEMENT_DETECTION_THRESHOLD) {
            if (!IndoorTracker.isIndoorTrackerRunning() && !OutdoorTracker.isOutdoorTrackerRunning()) {
                Log.d(Config.TAG, "Movement confirmed. Begin indoor-outdoor decision.");
                detectIsIndoorOutdoor();
            } else {
                isDetectionInProcess = false;
            }
        } else {
            if (IndoorTracker.isIndoorTrackerRunning() || OutdoorTracker.isOutdoorTrackerRunning()) {
                Log.d(Config.TAG, "Stationary confirmed. Finish tracking.");
                IndoorTracker.stopTracking();
                OutdoorTracker.stopTracking();
            }
            isDetectionInProcess = false;
        }
    }

    private static ArrayList<Beacon> beaconList = new ArrayList<Beacon>();
    private static BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    /**
     * The function determines whether a user is in indoor or not, by using Bluetooth beacon scanning.
     * <br> - During the scan, accelerometer-based movement detection is inactivated as discovery duration exceeds Config.MOVEMENT_DETECTION_INTERVAL.
     */
    private static void detectIsIndoorOutdoor() {
        OnBluetoothDiscoveryListener onBluetoothDiscoveryListener = new OnBluetoothDiscoveryListener(context, bluetoothAdapter) {
            @Override
            public void onBluetoothDeviceFound(Beacon beacon) {
                Log.d(Config.TAG, "In/Out detection: device found: "
                        + beacon.getDevice().getAddress() + "(" + beacon.getRSSI() + ")");
                if (Config.isMatchingBeacon(beacon.getDevice().getAddress())) {
                    beaconList.add(beacon);
                }
            }

            @Override
            public void onBluetoothDiscoveryFinished() {
                Log.d(Config.TAG, "In/Out detection: discovery finished.");
                if (beaconList.size() > 0) {
                    IndoorTracker.startTracking(context);
                } else {
                    OutdoorTracker.startTracking(context);
                }

                beaconList.clear();
                isDetectionInProcess = false;
            }
        };
        onBluetoothDiscoveryListener.performDiscovery();
    }

    /**
     * This function measures the energy of phone's acceleration.
     * <br> - You may not need to modify this function.
     *
     * @param accDataList An ArrayList of collected AccData from accelerometer readings.
     * @return The energy of acceleration
     */
    private static double getAccEnergy(ArrayList<AccData> accDataList) {
        if (accDataList.size() == 0) return 0.0;

        // get norm_input = input/9.8
        for (AccData data : accDataList) {
            data.setX(data.getX() / 9.8);
            data.setY(data.getY() / 9.8);
            data.setZ(data.getZ() / 9.8);
        }

        // get DC = mean(norm_input)
        double totalX = 0, totalY = 0, totalZ = 0;
        for (AccData data : accDataList) {
            totalX += data.getX();
            totalY += data.getY();
            totalZ += data.getZ();
        }

        double meanX = totalX / accDataList.size();
        double meanY = totalY / accDataList.size();
        double meanZ = totalZ / accDataList.size();

        // get AC = norm_input - DC
        for (AccData data : accDataList) {
            data.setX(data.getX() - meanX);
            data.setY(data.getY() - meanY);
            data.setZ(data.getZ() - meanZ);
        }

        // get ACenergy
        Double[] dataColumns = new Double[accDataList.size()];

        int i = 0;
        for (AccData data : accDataList) {
            dataColumns[i] = data.getX() * data.getX() + data.getY() * data.getY() + data.getZ() * data.getZ();
            i++;
        }

        // get mean of ACenergy
        double mean = 0;
        for (double d : dataColumns) {
            mean += d;
        }

        return mean / dataColumns.length;
    }
}