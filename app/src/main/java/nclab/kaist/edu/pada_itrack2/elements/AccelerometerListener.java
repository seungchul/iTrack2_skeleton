package nclab.kaist.edu.pada_itrack2.elements;

import java.util.ArrayList;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * An accelerometer listener class for convenience.
 * <br> - On onStart(), it starts collecting accelerometer readings.
 * <br> - On onStop(), it finishes collecting accelerometer readings.
 * <br> - getAccList() returns a list of collected sensor data from onStart().
 */
public class AccelerometerListener implements SensorEventListener {
	private long lastTime;
	private SensorManager sensorManager;
	private Sensor accSensor;
	private ArrayList<AccData> accList = new ArrayList<AccData>();

	public AccelerometerListener(Context context) {
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	}

    /**
     * This function registers accelerometer event listener and initializes accList.
     */
	public void onStart() {
		if (accSensor != null) {
            sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_FASTEST);
            accList.clear();
        }
	}

    /**
     * This function unregisters the listener.
     */
	public void onStop() {
		if (sensorManager != null)
			sensorManager.unregisterListener(this);
	}

    /**
     * This function returns a list of accelerometer readings collected after onStart().
     * @return An ArrayList of AccData
     */
	public ArrayList<AccData> getAccList() {
		return accList;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent se) {
		long currentTime = System.currentTimeMillis();
		float x = se.values[0];
		float y = se.values[1];
		float z = se.values[2];

		if (System.currentTimeMillis() - lastTime > 10L) {
			accList.add(new AccData(x, y, z));
			lastTime = currentTime;
		}
	}
}
