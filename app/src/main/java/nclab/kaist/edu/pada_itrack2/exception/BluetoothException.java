package nclab.kaist.edu.pada_itrack2.exception;

/**
 * This class is to describe an exception occurred from Bluetooth-related errors.
 */
public class BluetoothException extends RuntimeException {
}
