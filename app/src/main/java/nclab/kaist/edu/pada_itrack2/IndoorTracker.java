package nclab.kaist.edu.pada_itrack2;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;

import nclab.kaist.edu.pada_itrack2.elements.Beacon;
import nclab.kaist.edu.pada_itrack2.elements.OnBluetoothDiscoveryListener;

/**
 * Indoor tracker based on Bluetooth beacon scanning
 * <br> - You may need to implement periodic Bluetooth discovery.
 */
public class IndoorTracker {
    private static Context context = null;
    private static boolean isTrackingOn = false;

    /**
     * It returns whether the tracker is currently running or not.
     *
     * @return
     */
    public static boolean isIndoorTrackerRunning() {
        return isTrackingOn;
    }

    /**
     * It initiates periodic beacon discovery to log user traces in indoor.
     *
     * @param context Android activity or service to bind some periodic actions
     */
    public static void startTracking(Context context) {
        if (!isTrackingOn) {
            IndoorTracker.context = context;

            /* TODO: Periodic Indoor Beacon Discovery
             * Use searchIndoorBeacon to perform a single beacon discovery.
             */

            isTrackingOn = true;
        }
    }

    /**
     * It terminates periodic beacon discovery
     */
    public static void stopTracking() {
        if (isTrackingOn) {
            btListener.tryCancelDiscovery();

            /* TODO: Stop Periodic Indoor Beacon Discovery
             * Here you needs to stop your periodic tracking functionality
             * You may need to check a NULL context
             */

            isTrackingOn = false;
            context = null;
        }
    }

    /**
     * On detected, the beacon is added to this list.
     */
    private static ArrayList<Beacon> beaconList = new ArrayList<Beacon>();

    /**
     * Bluetooth discovery listener.
     * It is set to a global variable, to cancel it on stopTracking().
     */
    private static OnBluetoothDiscoveryListener btListener;

    /**
     * It performs a single Bluetooth discovery to find near beacons.
     */
    private static void searchIndoorBeacon() {
        btListener = new OnBluetoothDiscoveryListener(context, BluetoothAdapter.getDefaultAdapter()) {
            @Override
            public void onBluetoothDeviceFound(Beacon beacon) {
                Log.d(Config.TAG, "indoorTracker: device found: "
                        + beacon.getDevice().getAddress() + "(" + beacon.getRSSI() + ")");
                if (Config.isMatchingBeacon(beacon.getDevice().getAddress())) {
                    beaconList.add(beacon);
                }
            }

            @Override
            public void onBluetoothDiscoveryFinished() {
                Log.d(Config.TAG, "indoorTracker: discovery finished.");
                if (beaconList.size() == 0) {
                    logIndoorLocation(null);
                } else {
                    Beacon nearestBeacon = beaconList.get(0);
                    for (Beacon beacon : beaconList) {
                        if (nearestBeacon.getRSSI() < beacon.getRSSI()) {
                            nearestBeacon = beacon;
                        }
                    }
                    logIndoorLocation(nearestBeacon);
                }
                beaconList.clear();
            }
        };
        btListener.performDiscovery();
    }

    /**
     * This function logs indoor location traces into a log file.
     * <br> - You may not need to modify this function.
     *
     * @param beacon nearest indoor beacon in the discovery
     */
    private static void logIndoorLocation(Beacon beacon) {
        if (context == null) return;

        SharedPreferences pref = context.getSharedPreferences(Config.PREF_NAME, Context.MODE_PRIVATE);

        if (pref.getBoolean(Config.PREF_IS_LOGGING_ON, false)) {
            // Logging
            String timeStr = Config.getCurrentTime();

            if (beacon == null) {
                Config.writeToFile(Config.LOG_FILE_NAME, "indoor," + timeStr + "," + "null"
                        + "," + "null" + "," + "0" + "\r\n");
                Log.d(Config.TAG, "Written:"
                        + "indoor," + timeStr + "," + "null" + "," + "null" + "," + "0");
            } else {
                Config.writeToFile(Config.LOG_FILE_NAME, "indoor," + timeStr + "," + beacon.getDevice().getAddress()
                        + "," + beacon.getDevice().getName() + "," + beacon.getRSSI() + "\r\n");
                Log.d(Config.TAG, "Written: indoor," + timeStr + "," + beacon.getDevice().getAddress()
                        + "," + beacon.getDevice().getName() + "," + beacon.getRSSI());
            }
        }
    }
}
