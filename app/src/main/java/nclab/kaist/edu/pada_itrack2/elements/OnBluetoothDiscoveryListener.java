package nclab.kaist.edu.pada_itrack2.elements;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import nclab.kaist.edu.pada_itrack2.Config;
import nclab.kaist.edu.pada_itrack2.MainActivity;
import nclab.kaist.edu.pada_itrack2.exception.BluetoothException;

/**
 * Bluetooth discovery listener for convenience.
 * <br> - It manages a BroadcastReceiver which listens to Bluetooth-related intents.
 * <br> - It provides some convenience in performing a Bluetooth discovery
 * <br> - Two abstract functions: onBluetoothDeviceFound and onBluetoothDiscoveryFinished.
 */
public abstract class OnBluetoothDiscoveryListener {
    private Context context;
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothReceiver;

    public OnBluetoothDiscoveryListener(Context context, BluetoothAdapter bluetoothAdapter) {
        this.context = context;
        this.bluetoothAdapter = bluetoothAdapter;
        this.bluetoothReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction(); //may need to chain this to a recognizing function
                if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    short RSSI = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);

                    onBluetoothDeviceFound(new Beacon(device, RSSI));
                }

                if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                    onBluetoothDiscoveryFinished();

                    context.unregisterReceiver(bluetoothReceiver);
                }
            }
        };
    }

    /**
     * This callback function is called when a devices is found during the discovery.
     * @param beacon Detected Bluetooth beacon
     */
    public abstract void onBluetoothDeviceFound(Beacon beacon);

    /**
     * This callback function is called when a discovery is finished.
     */
    public abstract void onBluetoothDiscoveryFinished();

    /**
     * It requests 'cancel' discovery to OnBluetoothDiscoveryListener.
     *
     * @return 1 when success, 0 when failure, and -1 in case of no discovery currently.
     */
    public int tryCancelDiscovery() {
        if (bluetoothAdapter.isDiscovering()) {
            if (bluetoothAdapter.cancelDiscovery()) {
                return 1;
            } else {
                return 0;
            }
        }
        return -1;
    }

    /**
     * This function performs a single Bluetooth discovery.
     * <br> - It requests a user to enable Bluetooth module, prepares for discovery, registers a listener, and performs the discovery.
     */
    public void performDiscovery() {
        if (bluetoothAdapter == null) {
            throw new BluetoothException();
        }

        Config.checkPermission(context, Manifest.permission.BLUETOOTH);
        Config.checkPermission(context, Manifest.permission.BLUETOOTH_ADMIN);

        if (!bluetoothAdapter.isEnabled()) {
            if (MainActivity.thisActivity != null) {
                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                MainActivity.thisActivity.startActivityForResult(enableBT, 0xBEEF);
            } else {
                Toast.makeText(context, "Please turn on Bluetooth.", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(bluetoothReceiver, intentFilter);

        bluetoothAdapter.startDiscovery();
    }
}
