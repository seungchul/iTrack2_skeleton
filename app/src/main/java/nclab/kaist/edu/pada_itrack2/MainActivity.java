package nclab.kaist.edu.pada_itrack2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


// TODO: You can freely add/modify any program code to the skeleton.

/**
 * Main activity of iTrack.
 * <br> - It has two buttons, one for continuous tracking and the other for logging user tracks.
 * <br> - You may not need to modify this class.
 */
public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private ToggleButton tbLogging;
    private ToggleButton tbSensing;

    public static MainActivity thisActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Auto-generation
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tbLogging = (ToggleButton) findViewById(R.id.tbLogging);
        tbSensing = (ToggleButton) findViewById(R.id.tbSensing);
        tbLogging.setOnCheckedChangeListener(this);
        tbSensing.setOnCheckedChangeListener(this);

        thisActivity = this;

        SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd_HHmmss", Locale.KOREA);
        Date currentTime = new Date();
        Config.LOG_FILE_NAME = formatter.format(currentTime) + "_logData";
    }

    @Override
    protected void onDestroy() {
        MovementDetector.stopDetection();
        thisActivity = null;

        super.onDestroy();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.tbLogging:
                if (isChecked) {
                    Log.d(Config.TAG, "Logging checked.");
                    turnOnLogging(true);
                } else {
                    Log.d(Config.TAG, "Logging unchecked.");
                    turnOnLogging(false);
                }
                break;
            case R.id.tbSensing:
                if (isChecked) {
                    Log.d(Config.TAG, "Sensing checked.");
                    MovementDetector.startDetection(getApplicationContext());
                } else {
                    Log.d(Config.TAG, "Sensing unchecked.");
                    MovementDetector.stopDetection();
                }
                break;
            default:
                break;
        }
    }

    private void turnOnLogging(boolean isLoggingOn) {
        // filename includes starting time
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd_HHmmss", Locale.KOREA);
        Date currentTime = new Date();
        Config.LOG_FILE_NAME = formatter.format(currentTime) + "_logData";

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ed.putBoolean(Config.PREF_IS_LOGGING_ON, isLoggingOn);
        ed.commit();
    }
}
